/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex03.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:54:57 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 16:08:55 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<math.h>
#include "../ex03/ft_recursive_power.c"

int	main(void)
{
	int	nb;
	int	power;

	nb = 0;
	power = 0;
	printf("Expected : %d\n", (int)pow(nb, power));
	printf("Result   : %d\n", ft_recursive_power(nb, power));
	nb = 5;
	power = 0;
	printf("Expected : %d\n", (int)pow(nb, power));
	printf("Result   : %d\n", ft_recursive_power(nb, power));
	nb = 5;
	power = 2;
	printf("Expected : %d\n", (int)pow(nb, power));
	printf("Result   : %d\n", ft_recursive_power(nb, power));
	nb = -3;
	power = 3;
	printf("Expected : %d\n", (int)pow(nb, power));
	printf("Result   : %d\n", ft_recursive_power(nb, power));
	nb = 9;
	power = -1;
	printf("Expected : %d\n", (int)pow(nb, power));
	printf("Result   : %d\n", ft_recursive_power(nb, power));
	nb = 9;
	power = 5;
	printf("Expected : %d\n", (int)pow(nb, power));
	printf("Result   : %d\n", ft_recursive_power(nb, power));
}
