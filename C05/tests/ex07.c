/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex07.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 18:32:26 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/12 12:35:45 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex07/ft_find_next_prime.c"

int	main(void)
{
	printf("Expected : %d\n", 7);
	printf("Result   : %d\n",ft_find_next_prime(7));
	printf("Expected : %d\n", 2);
	printf("Result   : %d\n",ft_find_next_prime(-1000));
	printf("Expected : %d\n", 1025081);
	printf("Result   : %d\n",ft_find_next_prime(1025050));
	printf("Expected : %d\n", 2147483629);
	printf("Result   : %d\n",ft_find_next_prime(2147483600));
}
