/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 12:11:09 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 17:28:49 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

int		error_management(char *base);

char	put_dec_to_base_recursiv(unsigned int n, char *base);

int		ft_strlen(char *str);

void	ft_putnbr_base(int nbr, char *base)
{
	char			c;
	unsigned int	n;

	if (error_management(base))
		return ;
	n = nbr;
	if (nbr < 0)
	{
		write(1, "-", 1);
		n *= -1;
	}
	c = put_dec_to_base_recursiv(n, base);
	write(1, &c, 1);
}

int	error_management(char *base)
{
	int	i;
	int	j;

	i = -1;
	if (ft_strlen(base) <= 1)
		return (1);
	while (base[++i])
	{
		if (base[i] == '+' || base[i] == '-')
			return (1);
		j = -1;
		while (base[++j])
			if (base[i] == base[j] && i != j)
				return (1);
	}
	return (0);
}

char	put_dec_to_base_recursiv(unsigned int n, char *base)
{
	int		mod;
	char	c;

	mod = n % ft_strlen(base);
	n /= ft_strlen(base);
	if (n != 0)
	{
		c = put_dec_to_base_recursiv(n, base);
		write(1, &c, 1);
	}
	return (base[mod]);
}

int	ft_strlen(char *str)
{
	int	counter;

	counter = -1;
	while (str[++counter] != '\0')
		continue ;
	return (counter);
}
