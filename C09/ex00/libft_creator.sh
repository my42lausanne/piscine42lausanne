#!/bin/sh

FILES="ft_putchar.c ft_putstr.c ft_strcmp.c ft_strlen.c ft_swap.c"
OBJECTS="ft_putchar.o ft_putstr.o ft_strcmp.o ft_strlen.o ft_swap.o"

gcc -Wall -Wextra -Werror -c $FILES

ar rc libft.a $OBJECTS
