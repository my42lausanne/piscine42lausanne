/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex00.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/18 12:15:25 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/18 12:18:58 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// Command to launch lib on zsh :
// gcc -Wall -Wextra -Werror ex00.c -L../ex00 -lft

void	ft_putchar(char c);
void	ft_swap(int *a, int *b);
void	ft_putstr(char *str);
int		ft_strlen(char *str);
int		ft_strcmp(char *s1, char *s2);

int	main(void)
{
	ft_putstr("Hello World!\n");
}
