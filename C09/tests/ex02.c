/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex02.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/18 15:42:05 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/18 16:43:52 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>
#include "../ex02/ft_split.c"

void	print_tab(char **tab);

int	main(void)
{
	print_tab(ft_split("  This,is a test,44441", " 4,"));
	print_tab(ft_split("This,is a test,2", " 4,"));
	print_tab(ft_split("Continuous -- Only newlines after this", ""));
	print_tab(ft_split("", ""));
	print_tab(ft_split("", "asasdad"));
	print_tab(ft_split("aaaaaaaasasaaaaaad", "asasdad"));
}

void	print_tab(char **tab)
{
	int	i;

	i = -1;
	while (tab[++i])
	{
		if (!strlen(tab[i]))
			printf("ERROR -- Empty string in tab - %d\n", i);
		printf("%d --- %s\n", i, tab[i]);
	}
	printf("\n");
}
