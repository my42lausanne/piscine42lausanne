/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 19:09:14 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/12 10:44:02 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

void	ft_sort_tab(char *tab[], int size);

int		ft_strcmp(char *s1, char *s2);

void	ft_putstr(char *str);

int	main(int argc, char *argv[])
{
	int	i;

	ft_sort_tab(argv, argc);
	i = 0;
	while (++i < argc)
	{
		ft_putstr(argv[i]);
		write(1, "\n", 1);
	}
}

// BUBBLE SORT ALGORITHM
void	ft_sort_tab(char *tab[], int size)
{
	int		i;
	char	*temp;
	int		has_modified;

	has_modified = 1;
	while (has_modified)
	{
		i = 1;
		has_modified = 0;
		while (++i < size)
		{
			if (ft_strcmp(tab[i - 1], tab[i]) > 0)
			{
				temp = tab[i];
				tab[i] = tab[i - 1];
				tab[i - 1] = temp;
				has_modified = 1;
			}
		}	
	}
}

int	ft_strcmp(char *s1, char *s2)
{
	int	i;
	int	c;
	int	is_done;

	i = -1;
	is_done = 0;
	while (!is_done)
	{
		if (!(s1[++i] && s2[i]))
			is_done = 1;
		c = s1[i] - s2[i];
		if (c)
			return (c);
	}
	return (0);
}

void	ft_putstr(char *str)
{
	int	i;

	i = -1;
	while (str[++i] != '\0')
		write(1, &str[i], 1);
}
