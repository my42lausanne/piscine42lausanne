/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/17 16:55:23 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/17 17:57:08 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>
#include "ft_stock_str.h"
void	ft_print_char(int *i, int level, int *has_started);

void	ft_putnbr(int nb);

void	ft_putstr(char *str);

void	ft_show_tab(struct s_stock_str *par)
{
	int	i;

	i = -1;
	while (par[++i].str)
	{
		ft_putstr(par[i].str);
		write(1, "\n", 1);
		ft_putnbr(par[i].size);
		write(1, "\n", 1);
		ft_putstr(par[i].copy);
		write(1, "\n", 1);
	}
}

void	ft_putstr(char *str)
{
	int	i;

	i = -1;
	while (str[++i] != '\0')
		write(1, &str[i], 1);
}

void	ft_putnbr(int nb)
{
	int		level;
	int		has_started;
	int		is_negative;

	level = 1000000000;
	has_started = 0;
	is_negative = 0;
	if (nb < 0)
	{
		is_negative = 1;
		nb = (nb * -1) - 1;
		write(1, "-", 1);
	}
	else if (nb == 0)
		write(1, "0", 1);
	while (level > 0)
	{
		ft_print_char(&nb, level, &has_started);
		if (is_negative && level == 1000000000)
			nb++;
		level /= 10;
	}
}

void	ft_print_char(int *nb, int level, int *has_started)
{
	char	c;

	c = ((int) *nb / level) + '0';
	*nb = *nb % level;
	if (c != '0' || *has_started)
	{
		*has_started = 1;
		write(1, &c, 1);
	}
}
