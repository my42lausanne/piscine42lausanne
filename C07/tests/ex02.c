/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex02.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/17 09:57:03 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/17 10:55:26 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include<stdio.h>
#include "../ex02/ft_ultimate_range.c"

void	print_int_array(int n[], int size);

int	main(void)
{
	int	*range;
	int min = 0;
	int max = 25;
	
	range = 0;
	printf("Min - included : %d\n", min);
	printf("Max - excluded : %d\n", max);
	printf("Returned value : %d\n", ft_ultimate_range(&range, min, max));
	print_int_array(range, max - min);
	min = 10;
	max = 5;
	printf("Min - included : %d\n", min);
	printf("Max - excluded : %d\n", max);
	printf("Returned value : %d\n", ft_ultimate_range(&range, min, max));
}

void	print_int_array(int n[], int size)
{
	int i;

	i = -1;
	while (++i < size)
		printf("%d , ", n[i]);
	printf("\nend - %d\n", n[i]);
}
