/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/17 09:52:34 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/17 10:56:49 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int	*ptr;
	int	i;

	*range = 0;
	if (min >= max)
	{
		range = NULL;
		return (0);
	}
	ptr = malloc(sizeof(int) * (max - min));
	*range = ptr;
	if (ptr == NULL)
		return (-1);
	i = -1;
	while (++i + min < max)
		ptr[i] = i + min;
	return (max - min);
}
