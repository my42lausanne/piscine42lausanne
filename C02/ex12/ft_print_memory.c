/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/08 13:39:34 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/09 17:55:34 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

void	print_memaddr(long addr);

char	put_dec_to_hex_recursiv(long n, int *counter);

int		print_char_hex(char *str, int *i, unsigned int size);

void	print_chars(char *str, int start);

void	*ft_print_memory(void *addr, unsigned int size)
{
	int		i;
	int		j;
	int		is_done;
	char	*str;

	i = -1;
	is_done = 0;
	str = addr;
	while (!is_done && i + 1 < (int)size)
	{
		j = i;
		print_memaddr((long) &str[i]);
		is_done = print_char_hex(str, &i, size);
		i--;
		print_chars(str, j);
		write(1, "\n", 1);
	}
	return (addr);
}

void	print_memaddr(long addr)
{
	int		counter;
	char	c;

	counter = 0;
	c = put_dec_to_hex_recursiv(addr, &counter);
	write(1, &c, 1);
	write(1, ":", 1);
}

char	put_dec_to_hex_recursiv(long n, int *counter)
{
	int		mod;
	int		i;
	char	c;

	mod = n % 16;
	i = -1;
	++*counter;
	n /= 16;
	if (n != 0)
	{
		c = put_dec_to_hex_recursiv(n, counter);
		write(1, &c, 1);
	}
	else
		while (++i < 16 - *counter)
			write(1, "0", 1);
	if (mod <= 9)
		return (mod + '0');
	return ('a' + mod - 10);
}

int	print_char_hex(char *str, int *i, unsigned int size)
{
	char	c;
	int		j;
	int		counter;
	int		mod;

	j = -1;
	counter = 256;
	while (++*i < (int)size && ++j < 16)
	{
		if (j % 2 == 0)
			write(1, " ", 1);
		if (str[*i] + (256 * (str[*i]) < 0) < 16)
			write(1, "0", 1);
		c = put_dec_to_hex_recursiv(str[*i] + (256 * (str[*i]) < 0), &counter);
		write(1, &c, 1);
		if (str[*i] == '\0')
		{
			mod = 5 * ((16 - ((*i + 1) % 16)) * 2) / 4;
			while (mod--)
				write(1, " ", 1);
			return (1);
		}
	}
	return (0);
}

void	print_chars(char *str, int start)
{
	int	i;
	int	is_done;

	i = start;
	is_done = 0;
	write(1, " ", 1);
	while (!is_done && ++i <= start + 16)
	{
		if (!(' ' <= str[i] && str[i] <= '~'))
			write(1, ".", 1);
		else
			write(1, &str[i], 1);
		if (str[i] == '\0')
			is_done = 1;
	}
}
