/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/08 11:21:28 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/09 17:15:52 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

char	put_dec_to_hex_recursiv(int n);

int		is_printable(char c);

void	ft_putstr_non_printable(char *str)
{
	int		i;
	int		n;
	char	c;

	i = -1;
	while (str[++i] != '\0')
	{
		if (!is_printable(str[i]))
		{
			write(1, "\\", 1);
			n = str[i];
			if (n < 0)
				n += 256;
			if (n < 16)
				write(1, "0", 1);
			c = put_dec_to_hex_recursiv(n);
			write(1, &c, 1);
		}
		else
			write(1, &str[i], 1);
	}
}

int	is_printable(char c)
{
	return (' ' <= c && c <= '~');
}

char	put_dec_to_hex_recursiv(int n)
{
	int		mod;
	char	c;

	mod = n % 16;
	n /= 16;
	if (n != 0)
	{
		c = put_dec_to_hex_recursiv(n);
		write(1, &c, 1);
	}
	if (mod <= 9)
		return (mod + '0');
	return ('a' + mod - 10);
}
