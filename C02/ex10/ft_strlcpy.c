/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/08 10:26:13 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/09 16:33:33 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strlen(char *str);

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	int				i;
	unsigned int	dest_size;

	i = -1;
	dest_size = ft_strlen(dest);
	if (dest_size < size)
	{
		dest[dest_size] = '\0';
		size = dest_size;
	}
	else
		dest[size] = '\0';
	while ((unsigned int)++i < size)
		dest[i] = src[i];
	return (ft_strlen(dest));
}

int	ft_strlen(char *str)
{
	int	counter;

	counter = -1;
	while (str[++counter] != '\0')
		continue ;
	return (counter);
}
