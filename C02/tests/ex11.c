/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex11.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/08 11:29:23 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/09 17:18:57 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex11/ft_putstr_non_printable.c"

int	main(void)
{
	ft_putstr_non_printable("Expected : \\01\\06\\16\\83\\a5\\ff\\fa\\bd");
	printf("\n");
	ft_putstr_non_printable("Result   : \x01\x06\x16\x83\xa5\xff\xfa\xbd");
	printf("\n");
	ft_putstr_non_printable("This is a test\e - hex : \\1b");
	printf("\n");
	ft_putstr_non_printable("This is a test\b - hex : \\08");
	printf("\n");
	ft_putstr_non_printable("This is a test\t - hex : \\09");
	printf("\n");
	ft_putstr_non_printable("This is a test\r - hex : \\0d");
	printf("\n");
}
