/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex08.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/07 14:35:55 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/07 15:52:21 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex08/ft_strlowcase.c"

int	main(void)
{
	char str[] = "TESTSTRING MiXeD /*-";
	ft_strlowcase(str);
	printf("%s\n", str);
}

