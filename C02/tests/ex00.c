/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex00.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/05 19:14:47 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/09 16:21:12 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex00/ft_strcpy.c"

int	main(void)
{
	char	src[22];
	char	dest[25] = "0123456789012345678901234";
	char	*src_ptr;
	char	*dest_ptr;

	src_ptr = ft_strcpy(src, "String copying works!\0a2");
	dest_ptr = ft_strcpy(dest, src);
	printf("%s", src);
	printf("\nsrc pointer  : %p\n", &src_ptr);
	printf("dest pointer : %p\n", &dest_ptr);
}	
