/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex07.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/07 14:35:55 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/07 15:19:58 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex07/ft_strupcase.c"

int	main(void)
{
	char str[] = "teststring MiXeD /*-";
	ft_strupcase(str);
	printf("%s\n", str);
	//printf("%s\n", ft_strupcase("This Is A Mixed String"));
	//printf("Expected : /=+()# , Result : %s\n" ,ft_strupcase("/=+()#"));
}

