/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 14:19:57 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/09 16:25:25 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	unsigned int	i;
	int				found_term;

	i = -1;
	found_term = 0;
	while (++i < n)
	{
		found_term = (found_term || src[i] == '\0');
		if (found_term)
			dest[i] = '\0';
		else
			dest[i] = src[i];
	}
	return (dest);
}
