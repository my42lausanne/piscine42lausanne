#include "ft.h"
#include "load_dict.h"
#include "nbr_to_word.h"

int	print_word(unsigned int nbr, t_dict *dict, unsigned int space)
{
	int	index;

	index = find_in_dict(nbr, dict);
	if (index < 0)
		return (ft_error(DICT_ERROR));
	if (space)
		ft_putstr(" ");
	ft_putstr(dict[index].str);
	return (0);
}
