#include "ft.h"
#include "load_dict.h"
#include "nbr_to_word.h"

void	verify_hundreds(unsigned int nbr, t_dict *dict, int *error)
{
	if (nbr / 100)
	{
		verify_tens(nbr / 100, dict, error);
		if (find_in_dict(100, dict) < 0)
			(*error)++;
	}
	if (nbr % 100 > 0)
		verify_tens(nbr % 100, dict, error);
}
