#include "load_dict.h"

void	free_dict(t_dict *dict_list)
{
	int	is_done;
	int	i;

	is_done = 0;
	i = -1;
	while (!is_done)
	{
		if (!dict_list[++i].str)
			is_done = 1;
		free(dict_list[i].str);
	}
	free(dict_list);
}
