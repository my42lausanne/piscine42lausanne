#include "ft.h"
#include "nbr_to_word.h"
#include "load_dict.h"

int	print_nbr_to_word(unsigned int nbr, char *filename)
{
	t_dict	*dict;

	dict = load_dict(filename);
	if (!dict)
		return (ft_error(DICT_ERROR));
	if (verify_dict(nbr, dict))
		return (ft_error(DICT_ERROR));
	if (nbr == 0)
		print_word(0, dict, 0);
	if (nbr <= 999)
		print_hundreds(nbr, dict);
	if (nbr > 999 && nbr <= 999999)
		print_thousands(nbr, dict);
	if (nbr > 999999 && nbr <= 999999999)
		print_millions(nbr, dict);
	if (nbr > 999999999)
		print_billions(nbr, dict);
	ft_putstr("\n");
	free_dict(dict);
	return (0);
}
