#include "ft.h"

int	ft_power_ten(int n)
{
	if (n == 0)
		return (1);
	return (10 * ft_power_ten(n - 1));
}
