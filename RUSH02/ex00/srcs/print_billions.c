#include "ft.h"
#include "nbr_to_word.h"
#include "load_dict.h"

void	print_billions(unsigned int nbr, t_dict *dict)
{
	print_hundreds(nbr / 1000000000, dict);
	print_word(1000000000, dict, 1);
	if (nbr % 1000000000 > 0)
	{
		ft_putstr(" ");
		print_millions(nbr % 1000000000, dict);
	}
}
