#include "load_map.h"
#include "ft.h"

int		create_matrix(t_map *map);
int		testline_chars(char *str, t_map *map);
int		create_lines(t_map *map);
int		line_width_condition(char *str, t_map *map, int i);
void	line_copy(char *str, char **matrix, int nrow, int size);

int	parse_map(t_map *map, char *str)
{
	int	i;
	int	j;

	i = -2;
	j = -1;
	while (str[++i + 1] != '\n')
		;
	if ((unsigned int)count_line(&str[i + 1]) != map->height)
		return (1);
	while (str[++i])
	{
		if (!i || str[i - 1] == '\n')
		{
			if (j == -1)
			{
				map->width = ft_linelen(&str[i]);
				if (create_matrix(map))
					return (1);
			}
			if (line_width_condition(str, map, i))
				return (1);
			line_copy(&str[i], map->matrix, ++j, map->width);
		}
	}
	return (0);
}

void	line_copy(char *str, char **matrix, int nrow, int size)
{
	int	i;

	i = -1;
	while (++i < size)
		matrix[i][nrow] = str[i];
}

int	line_width_condition(char *str, t_map *map, int i)
{
	return ((unsigned int) ft_linelen(&str[i]) != map->width
		|| !testline_chars(&str[i], map));
}

int	create_matrix(t_map *map)
{
	int	i;

	map->matrix = malloc(map->width * sizeof(char *));
	if (!map->matrix)
		return (1);
	i = -1;
	while ((unsigned int)++i < map->width)
	{
		map->matrix[i] = malloc(sizeof(char) * map->height);
		if (!map->matrix[i])
			return (1);
	}
	return (0);
}

int	testline_chars(char *str, t_map *map)
{
	int	i;

	i = -1;
	while (str[++i] != '\0' && str[i] != '\n')
		if (str[i] != map->empty
			&& str[i] != map->obstacle)
			return (0);
	return (1);
}
