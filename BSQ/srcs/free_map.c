#include "load_map.h"

void	free_map(t_map *map)
{
	int	i;

	i = -1;
	while ((unsigned int)++i < map->width)
		free(map->matrix[i]);
	free(map->matrix);
	free(map);
}
