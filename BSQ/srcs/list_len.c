#include "stdin_to_str.h"

int	list_len(t_char_list *item)
{
	if (item->next)
		return (1 + list_len(item->next));
	return (1);
}
