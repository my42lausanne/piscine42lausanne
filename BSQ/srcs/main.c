/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gaubert <gaubert@student.42lausanne.ch>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/23 11:33:58 by gaubert           #+#    #+#             */
/*   Updated: 2021/08/25 16:40:48 by gaubert          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft.h"

void	debug(t_map *map)
{
	ft_putstr("Maxval: ");
	ft_putnbr(map->maxval);
	ft_putstr(" Max j: ");
	ft_putnbr(map->maxj);
	ft_putstr(" Max i: ");
	ft_putnbr(map->maxi);
	ft_putstr("\n");
}

int	process(char *file)
{
	t_map	*map;

	map = load_map(file, 0);
	if (map == 0)
	{
		write(2, "map error\n", 10);
		return (0);
	}
	if (!ft_get_val_matrix(map))
		return (0);
	ft_generate_output(map);
	ft_print_char_array(map->matrix, map->height, map->width);
	free_map(map);
	return (1);
}

int	processstdin(void)
{
	t_map	*map;

	map = load_map("", 1);
	if (map == 0)
	{
		write(2, "map error\n", 10);
		return (0);
	}
	if (!ft_get_val_matrix(map))
		return (0);
	ft_generate_output(map);
	ft_print_char_array(map->matrix, map->height, map->width);
	free_map(map);
	return (1);
}

int	main(int ac, char *av[])
{
	int	i;

	if (ac < 2)
		if (!processstdin())
			return (1);
	i = 0;
	while (++i < ac)
	{
		if (!process(av[i]))
			return (1);
	}
	return (0);
}
