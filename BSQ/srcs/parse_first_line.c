#include "load_map.h"
#include "ft.h"

int	char_is_printable(char c);
int	is_char_repeated(char c1, char c2, char c3);

int	parse_first_line(t_map *map, char *str)
{
	long	atoi;
	int		i;

	atoi = ft_atoi(str);
	if (atoi < 0)
		return (1);
	map->height = atoi;
	i = -1;
	while ('0' <= str[++i] && str[i] <= '9')
		;
	--i;
	if (char_is_printable(str[i + 1])
		&& char_is_printable(str[i + 2])
		&& char_is_printable(str[i + 3]))
	{
		map->empty = str[++i];
		map->obstacle = str[++i];
		map->full = str[++i];
	}
	else
		return (1);
	if (str[++i] != '\n'
		|| is_char_repeated(map->empty, map->obstacle, map->full))
		return (1);
	return (0);
}

int	char_is_printable(char c)
{
	return (' ' <= c && c <= '~');
}

int	is_char_repeated(char c1, char c2, char c3)
{
	if (c1 == c2 || c1 == c3 || c2 == c3)
		return (1);
	return (0);
}
