/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_matrix.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gaubert <gaubert@student.42lausanne.ch>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/23 13:16:39 by gaubert           #+#    #+#             */
/*   Updated: 2021/08/25 16:35:54 by gaubert          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int	min(int i, int j, int k)
{
	int	small;

	small = INT32_MAX;
	if (i < small)
		small = i;
	if (j < small)
		small = j;
	if (k < small)
		small = k;
	return (small);
}

void	ft_set_val(t_map *map, int **mat, int i, int j)
{
	if (map->matrix[j][i] == map->obstacle)
		mat[j][i] = 0;
	else if (i == 0 || j == 0)
	{
		if (map->matrix[j][i] == map->empty)
		{
			mat[j][i] = 1;
			if (map->maxval < mat[j][i])
			{
				map->maxval = mat[j][i];
				map->maxi = i;
				map->maxj = j;
			}
		}
	}
	else
	{
		mat[j][i] = 1 + min(mat[j - 1][i - 1], mat[j - 1][i], mat[j][i - 1]);
		if (map->maxval < mat[j][i])
		{
			map->maxval = mat[j][i];
			map->maxi = i;
			map->maxj = j;
		}
	}
}

int	**ft_generate_work_matrix(int height, int width)
{
	int	**matrix;
	int	i;

	matrix = (int **)malloc(sizeof(int *) * width + 1);
	if (!matrix)
		return (0);
	i = -1;
	while (++i < width)
	{
		matrix[i] = (int *)malloc(sizeof(int) * height + 1);
		if (!matrix[i])
			return (0);
	}
	return (matrix);
}

int	ft_get_val_matrix(t_map *map)
{
	int	**matrix;
	int	i;
	int	j;

	matrix = ft_generate_work_matrix(map->height, map->width);
	if (!matrix)
		return (0);
	i = -1;
	map->maxval = 0;
	while ((unsigned int)++i < map->height)
	{
		j = -1;
		while ((unsigned int)++j < map->width)
		{
			ft_set_val(map, matrix, i, j);
		}
	}
	i = -1;
	while ((unsigned int)++i < map->width)
		free(matrix[i]);
	free(matrix);
	return (1);
}

void	ft_generate_output(t_map *map)
{
	int	i;
	int	j;

	i = -1;
	while (++i < map->maxval)
	{
		j = -1;
		while (++j < map->maxval)
		{
			map->matrix[map->maxj - j][map->maxi - i] = map->full;
		}
	}
}
