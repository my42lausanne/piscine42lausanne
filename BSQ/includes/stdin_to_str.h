#ifndef STDIN_TO_STR_H
# define STDIN_TO_STR_H
# include "load_map.h"

typedef struct s_char_list
{
	char				c;
	struct s_char_list	*next;
}	t_char_list;

t_char_list	*list_append(t_char_list *item, char c_ap);
int			list_len(t_char_list *item);
char		*list_to_str(t_char_list *item, char *str);
char		*stdin_to_str(t_map *map);

#endif
