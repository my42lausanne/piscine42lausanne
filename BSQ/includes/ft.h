#ifndef FT_H
# define FT_H
# include "load_map.h"
int		count_line(char *str);
int		count_nbrs(char *str);
long	ft_atoi(char *str);
int		ft_power_ten(int n);
void	ft_print_int_array(int **matrix, int height, int length);
void	ft_print_char_array(char **matrix, int height, int length);
void	ft_putstr(char *str);
int		ft_strlen(char *str);
void	ft_putnbr(int nbr);
int		ft_get_val_matrix(t_map *map);
void	ft_generate_output(t_map *map);
#endif