/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/04 17:05:18 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/05 17:09:17 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

void	ft_putchar(char c);

void	ft_print_char(int *i, int level, int *has_started);

// MAX INT VALUE IN C: 2147483647
// MIN INT VALUE IN C: -2147483648
void	ft_putnbr(int nb)
{
	int		level;
	int		has_started;
	int		is_negative;

	level = 1000000000;
	has_started = 0;
	is_negative = 0;
	if (nb < 0)
	{
		is_negative = 1;
		nb = (nb * -1) - 1;
		ft_putchar('-');
	}
	else if (nb == 0)
		ft_putchar('0');
	while (level > 0)
	{
		ft_print_char(&nb, level, &has_started);
		if (is_negative && level == 1000000000)
			nb++;
		level /= 10;
	}
}

void	ft_print_char(int *nb, int level, int *has_started)
{
	char	c;

	c = ((int) *nb / level) + '0';
	*nb = *nb % level;
	if (c != '0' || *has_started)
	{
		*has_started = 1;
		ft_putchar(c);
	}
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}
