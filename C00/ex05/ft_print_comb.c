/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/04 13:09:11 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/05 10:42:07 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

void	ft_putchar(char c);

void	ft_print_result(char i, char j, char k, int is_first);

void	ft_print_comb(void)
{
	int	is_first;
	int	i;
	int	j;
	int	k;

	is_first = 1;
	i = 0;
	while (i <= 9)
	{
		j = i + 1;
		while (j <= 9)
		{
			k = j + 1;
			while (k <= 9)
			{
				ft_print_result('0' + i, '0' + j, '0' + k, is_first);
				is_first = 0;
				k++;
			}
			j++;
		}
		i++;
	}
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_print_result(char i, char j, char k, int is_first)
{
	if (!is_first)
		write(1, ", ", 2);
	ft_putchar(i);
	ft_putchar(j);
	ft_putchar(k);
}
