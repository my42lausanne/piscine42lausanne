/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 10:13:51 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 10:04:25 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str);

char	*ft_strncat(char *dest, char *src, unsigned int n)
{
	unsigned int	i;
	int				dest_size;
	int				is_done;

	i = -1;
	dest_size = ft_strlen(dest) - 1;
	is_done = 0;
	while (!is_done && ++i < n)
	{
		if (!src[i])
			is_done = 1;
		dest[++dest_size] = src[i];
	}
	if (!is_done)
		dest[dest_size + 1] = 0;
	return (dest);
}

int	ft_strlen(char *str)
{
	int	counter;

	counter = -1;
	while (str[++counter] != '\0')
		continue ;
	return (counter);
}
