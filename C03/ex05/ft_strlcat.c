/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 10:13:51 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 10:50:32 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str);

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	int	i;
	int	dest_size;

	i = -1;
	dest_size = ft_strlen(dest);
	while (src[++i] && i < (int)size - dest_size - 1)
	{
		dest[dest_size + i] = src[i];
	}
	if (size > 0 && size > (unsigned int) dest_size)
		dest[size - 1] = 0;
	else
		return (ft_strlen(src) + size);
	return (dest_size + ft_strlen(src));
}

int	ft_strlen(char *str)
{
	int	counter;

	counter = -1;
	while (str[++counter] != '\0')
		;
	return (counter);
}
