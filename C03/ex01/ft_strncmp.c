/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/09 14:13:57 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/10 10:01:57 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int	i;
	int				c;
	int				is_done;

	i = -1;
	is_done = 0;
	while (!is_done && ++i < n)
	{
		if (!(s1[i] && s2[i]))
			is_done = 1;
		c = s1[i] - s2[i];
		if (c)
			return (c);
	}
	return (0);
}
