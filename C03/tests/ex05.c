/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex05.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 10:14:39 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 10:49:56 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>
#include "../ex05/ft_strlcat.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	char			c1[100];
	char			c2[100];
	unsigned int	r;

	ft_strcpy(c1, "0123456789");
	ft_strcpy(c2, "45\0xadsa");
	r = strlcat(c1, c2, 11);
	printf("Expected : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "0123456789");
	r = ft_strlcat(c1, c2, 11);
	printf("Result   : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "0123456789");
	ft_strcpy(c2, "45\0xadsa");
	r = strlcat(c1, c2, 50);
	printf("Expected : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "0123456789");
	r = ft_strlcat(c1, c2, 50);
	printf("Result   : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "0123");
	ft_strcpy(c2, "4567890123456789");
	r = strlcat(c1, c2, 8);
	printf("Expected : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "0123");
	r = ft_strlcat(c1, c2, 8);
	printf("Result   : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "This is a \0test");
	ft_strcpy(c2, "TEST");
	r = strlcat(c1, c2, 15);
	printf("Expected : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "This is a \0test");
	r = ft_strlcat(c1, c2, 15);
	printf("Result   : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "0123456789");
	ft_strcpy(c2, "0123");
	r = strlcat(c1, c2, 18);
	printf("Expected : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "0123456789");
	r = ft_strlcat(c1, c2, 18);
	printf("Result   : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "abc");
	ft_strcpy(c2, "xx");
	r = strlcat(c1, c2, 0);
	printf("Expected : %s - Re %d\n", c1, r);
	ft_strcpy(c1, "abc");
	r = ft_strlcat(c1, c2, 0);
	printf("Result   : %s - Re %d\n", c1, r);
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
