/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex03.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 10:14:39 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 10:02:39 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>
#include "../ex03/ft_strncat.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	char	c1[100];
	char	c2[100];

	ft_strcpy(c1, "0123");
	ft_strcpy(c2, "45\0xadsa");
	printf("Expected : %s\n", strncat(c1, c2, 50));
	ft_strcpy(c1, "0123");
	printf("Result   : %s\n", ft_strncat(c1, c2, 50));
	ft_strcpy(c1, "0123");
	ft_strcpy(c2, "456789");
	printf("Expected : %s\n", strncat(c1, c2, 5));
	ft_strcpy(c1, "0123");
	printf("Result   : %s\n", ft_strncat(c1, c2, 5));
	ft_strcpy(c1, "00000000000000000000000");
	c1[3] = 0;
	ft_strcpy(c2, "abcdef\0");
	printf("Expected : %s\n", strncat(c1, c2, 3));
	ft_strcpy(c1, "00000000000000000000000");
	c1[3] = 0;
	printf("Result   : %s\n", ft_strncat(c1, c2, 3));
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
