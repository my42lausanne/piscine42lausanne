/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 10:13:51 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/10 12:31:13 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str);

char	*ft_strcat(char *dest, char *src)
{
	int	i;
	int	dest_size;
	int	is_done;

	i = -1;
	dest_size = ft_strlen(dest) - 1;
	is_done = 0;
	while (!is_done)
	{
		if (!src[++i])
			is_done = 1;
		dest[++dest_size] = src[i];
	}
	return (dest);
}

int	ft_strlen(char *str)
{
	int	counter;

	counter = -1;
	while (str[++counter] != '\0')
		continue ;
	return (counter);
}
